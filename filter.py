#!/usr/bin/python
import sys, os

SOURCE_SPP = "heinz2.40"


FORBID  = 0
REQUIR  = 1
IGNORE  = 2


class gff(object):
    def __init__(self, fn, desc=None, slack=1, minSize=1):
        self.fn        = fn
        self.slack     = slack
        self.minSize   = minSize

        self.fh        = open(fn, 'w')
        self.lastChrom = ""
        self.startPos  = 0
        self.finalPos  = 0

        self.indiCount = 0
        self.indiChrCo = {}
        self.featCount = 0
        self.featChrCo = {}


        self.fh.write("##gff-version 3\n")
        if desc is not None:
            self.fh.write(desc)
        self.fh.write("#seqId\tsource\ttype\tstart\tend\tscore\tstrand\tphase\tattributes\n")

    def write(self, chrom, pos):
        self.indiCount += 1
        try:
            self.indiChrCo[chrom] += 1
        except KeyError:
            self.indiChrCo[chrom]  = 1

        if chrom == self.lastChrom:
            if pos <= self.finalPos + self.slack:
                self.finalPos = pos

            else:
                self.writeData()
                self.startPos = pos
                self.finalPos = pos

        else:
            self.lastChrom = chrom
            self.startPos  = pos
            self.finalPos  = pos

    def writeData(self):
        if ( self.finalPos - self.startPos + 1 ) >= self.minSize:
            self.featCount += 1
            try:
                self.featChrCo[self.lastChrom] += 1
            except KeyError:
                self.featChrCo[self.lastChrom]  = 1

            dsc = "ID=%d;chr_id=%d;count=%d;chr_count=%d;NAME=%s" % (self.featCount, self.featChrCo[self.lastChrom], self.indiCount, self.indiChrCo[self.lastChrom], self.lastChrom + "_" + str(self.featChrCo[self.lastChrom]))
            #TODO: ADD STATISTICS
            #       seqId source type start end score strand phase attributes
            data = "%s\t%s\tgap\t%d\t%d\t.\t+\t.\t%s\n" % (self.lastChrom, SOURCE_SPP, self.startPos, self.finalPos, dsc)
            self.fh.write(data)

    def close(self):
        self.writeData()
        self.fh.close()

def main():
    minVal = 1

    if len(sys.argv) < 3:
        print "not enought arguments"
        print sys.argv[0], " <incov> <inlist>"
        sys.exit(1)

    if len(sys.argv) == 4:
        minval = sys.argv[3]

    incov   = sys.argv[1]
    inlist  = sys.argv[2]

    setup   = []
    files   = []
    descr   = []

    fileNum = 0

    with open(inlist, 'r') as inlistfh:
        for line in inlistfh:
            fileNum += 1
            data     = line.strip().split(",")

            setupnum = int( data[0] )

            filename = "#" + str( fileNum )
            descript = "#" + str( fileNum )
            if len (data) > 1:
                filename = data[1]

                if len(data) > 2:
                    descript = data[2]

            files.append( filename )
            descr.append( descript )

            if   setupnum ==  0: setup.append( IGNORE )
            elif setupnum ==  1: setup.append( REQUIR )
            elif setupnum == -1: setup.append( FORBID )
            else:
                print line
                print "wrong formating. can be 0, 1 -1"
                sys.exit(1)

    desc = ""
    for pairs in [(REQUIR, "Required"), (IGNORE, "Ignored"), (FORBID, "Forbidden")]:
        print pairs[1], ":"
        desc += "#" + pairs[1] + ":\n"

        for xpos in range( len( files ) ):
            if setup[ xpos ] == pairs[ 0 ]:
                print    "\t%s" % files[ xpos ],
                desc += "#\t%s" % files[ xpos ]
                if len(descr) > 0:
                    print   "\t%s" % descr[ xpos ],
                    desc += "\t%s" % descr[ xpos ]
                print
                desc += "\n"


    outgff = gff( inlist + '.filtered.gff3', desc, slack=5, minSize=50)

    lastChrom = None
    lastPos   = None

    with open(inlist + '.filtered.cov', 'w') as outlist:
        with open(incov, 'r') as incovfh:
            linecount  = 0
            validcount = 0

            for line in incovfh:
                #print line
                linecount  += 1
                ldata       = line.strip().split("\t")

                currChrom   =      ldata[0]
                currPos     = int( ldata[1] )

                if currChrom != lastChrom:
                    lastChrom = currChrom
                    print "\n", lastChrom

                if currPos %  10000 == 0:
                    print currPos,"",

                if currPos % 100000 == 0:
                    print "\n",lastChrom,


                data = [ int( x ) for x in ldata[2:] ]
                res  = True

                for xpos in range(len(data)):
                    #print " xpos", xpos,
                    linedata = data[xpos]

                    sre = setup[ xpos ]
                    #print " setup", sre, "val", linedata,

                    if   sre == IGNORE: # if ignore
                        pass

                    elif sre == REQUIR and ( linedata < minVal ): # if required and value too low, error, skip
                            #print " ",files[ xpos ],"setup", sre, "val", linedata,"required and value too low. skipping"
                            res = False
                            break

                    elif sre == FORBID and ( linedata >= minVal ): # if forbidden and value is ok, error, skip
                            #print " ",files[ xpos ],"setup", sre, "val", linedata," forbidden and value is ok. skipping"
                            res = False
                            break

                if res:
                    validcount += 1
                    print "+",
                    outlist.write( line )
                    outgff.write( currChrom, currPos )


    outgff.close()
    print
    print "total lines:", linecount
    print "valid lines:", validcount

if __name__ == '__main__': main()
