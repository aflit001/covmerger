if [[ -f covMerger ]]; then
	rm covMerger
fi

g++ -O3 covMerger.cpp -o covMerger -fexpensive-optimizations -ffast-math -floop-parallelize-all -fwhole-program -m64  -mmmx  -msse  -msse2 -msse3 -mssse3 -msse4.1 -msse4.2 -msse4
chmod +x covMerger
