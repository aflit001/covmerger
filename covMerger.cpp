#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <vector>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <cctype>
#include <map>
#include <math.h>

using namespace std;

template <typename T> // convert from string to number
T ston(const string str)
{
        T dSub;
        istringstream iss(str);
        iss >> dSub;
        return dSub;
};

int getMax(int argc, char **argv)
{
    if ( argc > 1 )
    {
        //cerr << "ARGC GT 1" << endl;
        //cerr << "  ARGC EQ 2" << endl;
        string inp = argv[1];
        int    val = ston<int>(inp);
        return val;
    } else {
        //cerr << "ARGC LT 1" << endl;
        return 0;
    }
}

bool getGap(int argc, char **argv)
{
    if ( argc > 1 )
    {
        //cerr << "ARGC GT 1" << endl;
        if ( argc == 3 )
        {
            //cerr << "  ARGC EQ 3" << endl;
            if ( strcmp(argv[2], "g") == 0 )
            {
                //cerr << "    THIRD IS G" << endl;
                return true;
            } else {
                //cerr << "    THIRD IS NOT G [" << argv[2] << "]" << endl;
                return false;
            }
        } else {
            //cerr << "  ARGC NE 2 [" << argc << "]" << endl;
            return false;
        }
    } else {
        //cerr << "ARGC LT 1" << endl;
        return false;
    }
}

typedef unsigned long long int    uint64_t;
typedef                    double udou64_t;
int CRJUSTCLOSED = 0;
int CRCLOSED     = 1;
int CRVALID      = 2;

struct covRegister {
    string   chr;
    uint64_t pos;
    int      cov;
    int      state;
};

//typedef vector<vector<vector<int> > > dataStruct;
typedef vector<string               > vecString;
typedef vector<bool                 > vecBool;
typedef vector<covRegister          > vecCovRegister;
typedef map<   string, int          > mapString;

struct  heapResult {
    vecCovRegister result;
    string         chr;
    int            pos;
    int            chrCount;
    int            posCount;
    int            sumCov;
};

class stats {
    private:
        uint64_t sum; // TODO: CONVERT TO LONG INT
        uint64_t count;
        udou64_t var;
    public:
        stats(): sum(0), count(0), var(0) {};

        double avg() {
            return float(sum) / float(count);
        };

        void add(int val) {
            sum += val;
            ++count;
            var +=  val - avg();
        };

        double getVar()    { return var;       };
        double gatStdDev() { return sqrt(var); };
        int    getSum()    { return sum;       };
        int    getCount()  { return count;     };
};

class covheap {
    typedef vector<ifstream*            > vecIfstream;

    private:
        vecString   fileNames;
        vecIfstream filehandle;
        vecBool     fileStates;
        mapString   filePos;
        int         numOpenFiles;

        string    line;
        string    token;
        vecString tokens;

        int            chromCount;
        int            posCount;
        int            lastPos;
        string         lastChr;
        vecCovRegister heapHead;

    public:
        covheap(): numOpenFiles(0), chromCount(0), posCount(0), lastPos(0), lastChr("") {
            tokens.resize(3);
        };

        void addFile( string fileName ) {
            filePos[ fileName ] = fileNames.size();

            fileNames.push_back( fileName );
            ifstream *infilehandle = new ifstream( fileName.c_str() );

            if ( infilehandle ) {
                if ( !infilehandle->is_open() ) {
                    cerr << "error reading file" << endl;
                    exit( 1 );
                }

                filehandle.push_back( infilehandle );
                fileStates.push_back( true         );
                ++numOpenFiles;
                covRegister firstRegister = getRegister( filePos[ fileName ] );
                heapHead.push_back( firstRegister );
            } else {
                cerr << "error opening file" << endl;
                exit( 1 );
            }
        };

        string getCurrChr() {
            return lastChr;
        };

        int getCurrpos(){
            return lastPos;
        };

        int getNumFiles() {
            return fileNames.size();
        };

        bool isempty() {
            //cerr << "is empty? " << numOpenFiles << "\n";
            return numOpenFiles == 0;
        };

        covRegister getRegister( int fileNum ) {
            ifstream    *infilehandle = filehandle[ fileNum ];
            covRegister  currRegister;

            if ( infilehandle->is_open() ) {
                if ( !infilehandle->eof() ) {
                    tokens.clear();
                    getline( (*infilehandle), line );
                    //cerr << line << endl;
                    istringstream iss(line);
                    int currTokenPos = 0;

                    while( getline(iss, token, '\t') ) {
                        tokens[currTokenPos] = token;
                        ++currTokenPos;
                    }

                    if ( currTokenPos == 3 ) {
                        string chr          = tokens[0];
                        int    pos          = ston<int>( tokens[1] );
                        int    cov          = ston<int>( tokens[2] );

                        currRegister.chr    = chr;
                        currRegister.pos    = pos;
                        currRegister.cov    = cov;
                        currRegister.state  = CRVALID;
                    } else {
                        return getRegister( fileNum ); // empty line. request next
                    }
                } else {
                    infilehandle->close();
                    fileStates[ fileNum ] = false;
                    --numOpenFiles;
                    currRegister.state  = CRJUSTCLOSED; // file is empty. close it and return empty
                }
            } else {
                fileStates[ fileNum ] = false;
                currRegister.state  = CRCLOSED; // file is closed. shouldn't have been requested
            }

            return currRegister;
        };

        heapResult next() {
            heapResult     currResult;

            vecCovRegister response;
            response.resize( fileNames.size() );

            chromCount = 0;
            posCount   = 0;

            int sumCov = 0;

            for ( int fileNum = 0; fileNum < fileNames.size(); ++fileNum ) {
                if ( fileStates[ fileNum ] ) { // if file not closed
                    if ( heapHead[ fileNum ].state == CRVALID ) { // is valid
                        if ( lastChr == "" ) {
                            lastChr = heapHead[ fileNum ].chr;
                            cerr << "STARTING CHROMOSOME " << lastChr << "\n";
                            //TODO: FIGURE OUT WHAT IS THE NEXT CHROMOSOME AMONG ALL THE OPTIONS
                            //      IN CASE A FILE SKIPS ONE CHROMOSOME
                        }

                        if ( heapHead[ fileNum ].chr == lastChr ) { // if chrom is correct
                            ++chromCount; // chrom still exists

                            if ( heapHead[ fileNum ].pos == lastPos ) { // if pos is correct
                                ++posCount;

                                covRegister cval     = heapHead[ fileNum ]; // get info
                                response[ fileNum ]  = cval;                // add to response
                                sumCov              += cval.cov;

                                covRegister nval    = getRegister( fileNum ); // get next line

                                heapHead[ fileNum ] = nval; // add to response
                            } // end if pos is correct
                        } // if chrom is correct
                    }
                } // end if file is open
            } // end for sppnum


            currResult.result   = response;
            currResult.chr      = lastChr;
            currResult.pos      = lastPos;
            currResult.chrCount = chromCount;
            currResult.posCount = posCount;
            currResult.sumCov   = sumCov;

            ++lastPos;

            if ( chromCount == 0 ) { // no more positions to current chromosome
                lastPos = 0;
                lastChr = "";
                return next();
            }

            return currResult;

            //string fileName = fileNames[ fileNum ];
            //cerr << "reading " << fileName << endl;
            //if ( chr != lastChromName ) {
            //    cerr << "chrom " << chr << endl;
            //    //cerr << "chrom " << chr << " not found " << endl;
            //    mapString::iterator it = chromPos.find(chr);
            //    if ( it != chromPos.end() ) {
            //        //cerr << "  chrom " << chr << " found in db" << endl;
            //
            //        lastChromPos  = it->second;
            //        lastChromName = chr;
            //    } else {
            //        //cerr << "  chrom " << chr << " not found in db. creating" << endl;
            //        //cerr << "    new pos " << chromNames.size() << endl;
            //        chromPos[chr] = chromNames.size();
            //        //cerr << "    added to chrompos" << endl;
            //        lastChromPos  = chromNames.size();
            //        //cerr << "    added to last chrom pos" << endl;
            //        lastChromName = chr;
            //        //cerr << "    added to last chrom name" << endl;
            //        chromNames.push_back(chr);
            //        //cerr << "    pushed to back of chrom name" << endl;
            //    }
            //}
        };
};

int main (int argc, char **argv)
{
    if ( argc < 4 ) {
        cerr << " needs at least 1 output file and 2 input files" << endl;
        return 1;
    }

    string      outFile = argv[1];

    cout << "saving to " << outFile << "\n";

    //dataStruct  data;

    vecString   chromNames;
    mapString   chromPos;

    covheap     heap;

    for ( int argnum = 2; argnum < argc; ++argnum ) {
        string infile = argv[argnum];
        cerr << "opening " << infile << endl;
        heap.addFile( infile );
    }

    stats     statistics; //TODO: PER CHROMOSOME

    int numFiles = heap.getNumFiles();

    ofstream outFileHandle;
    outFileHandle.open( outFile.c_str(), ios::out );

    if ( !outFileHandle ) {
        cerr << "error with output file\n";
        exit(1);
    }

    if ( !outFileHandle.is_open() ) {
        cerr << "error opening output file\n";
        exit(1);
    }

    //if ( !outFileHandle.is_good() ) {
    //    cerr << "error bad output file\n";
    //    exit(1);
    //}

    while ( !heap.isempty() ) {
        heapResult val = heap.next();

        //cerr << "CHR " << val.chr << " POS " << val.pos << " CHR COUNT " << val.chrCount << " POS COUNT " << val.posCount << " SUM COV " << val.sumCov << "\n";
        //for ( int fileNum = 0; fileNum < numFiles; ++fileNum ) {
        //    cerr << "  chr " << val.result[fileNum].chr << " pos " << val.result[fileNum].pos << " cov " << val.result[fileNum].cov << " state " << val.result[fileNum].state << "\n";
        //    if ( val.result[fileNum].cov > 0 ) {
        //        statistics.add( val.result[fileNum].cov );
        //    }
        //}

        outFileHandle << val.chr << "\t" << val.pos;

        for ( int fileNum = 0; fileNum < numFiles; ++fileNum ) {
            //outFileHandle << " cov " << val.result[fileNum].cov << " chr " << val.result[fileNum].chr << " pos " << val.result[fileNum].pos;
            outFileHandle << "\t" << val.result[fileNum].cov;
            if ( val.result[fileNum].cov > 0 ) {
                statistics.add( val.result[fileNum].cov );
            }
        }
        outFileHandle << "\n";
    }

    outFileHandle.close();


    cerr << "STATISTICS" << endl;
    cerr << "SUM   : "   << statistics.getSum()    << "\n";
    cerr << "AVG   : "   << statistics.avg()       << "\n";
    cerr << "COUNT : "   << statistics.getCount()  << "\n";
    cerr << "VAR   : "   << statistics.getVar()    << "\n";
    cerr << "STDDEV: "   << statistics.gatStdDev() << "\n";
    int sq1min = statistics.avg() - statistics.gatStdDev();
    if ( sq1min < 0 ) { sq1min = 1; };
    cerr << "1SQMIN: "   << sq1min << "\n";

    //for ( int chromPos = 0; chromPos < data.size(); ++chromPos ) {
    //    //data[ lastChromPos ][ pos ] += 1;
    //    string chromName = chromNames[ chromPos ];
    //    for ( int pos = 0; pos < data[ chromPos ].size(); ++pos ) {
    //        cout << chromName << "\t" << pos;
    //        int sum = 0;
    //        int num = 0;
    //        for ( int fileNum = 0; fileNum < data[ chromPos ][ pos ].size(); ++fileNum ) {
    //            int val = data[ chromPos ][ pos ][ fileNum ];
    //            cout << "\t" << val;
    //            sum += val;
    //            if ( val > 0 ) {
    //                ++num;
    //            }
    //        }
    //        cout << "\t" << sum << "\t" << num << "\n";
    //    }
    //}
}


//
//int main2 (int argc, char **argv)
//{
//    //vector<int> * svLines = new vector<int>;
//    vector<unsigned short> * svLines = new vector<unsigned short>;
//
//    int  iMax = getMax(argc, argv);
//    bool gap  = getGap(argc, argv);
//
//    if ( iMax > 0 )
//    {
//        //cerr  << "GOT MAX [" << iMax << "]" << endl;
//        svLines->resize(iMax, 0);
//    }
//
//
//    int lastEnd = 0;
//    while(!cin.eof() && cin.good()) {
//        string cinStr;
//        getline(cin, cinStr);
//        istringstream iss(cinStr);
//        //cout << "CINSTR " << cinStr << endl;
//
//        string sSub;
//        int j       =  0;
//        int begin   = -1;
//        int end     = -1;
//
//        while(getline(iss, sSub, '\t'))
//        { // split each tab
//            //cout << "  SUB " << sSub << endl;
//            if ( j == 0 )
//            { // begin
//                begin = ston<int>(sSub);
//            }
//            else if ( j == 1 )
//            { // end
//                end = ston<int>(sSub);
//            //} else {
//                //cerr << "SHOULD BE ONLY TWO COLUMNS. MORE FOUND";
//                //cerr << cLine;
//                //exit(1);
//            }
//            //double dSub = ston<double>(sSub);
//            j++;
//        }
//
//        if (( begin > -1 ) && ( end > -1  ))
//        {
//            if ( end <= begin )
//            {
//                cerr  << "BEGIN [" << begin << "] IS BIGGER THAN END [" << end
//                      << "]" << endl << ". ALTHOUGH I COULD DEAL WITH THAT, I'VE "
//                      << "CHOOSEN TO DIE AND LET YOU KNOW THAT YOUR DATA IS "
//                      << "WRONG" << endl;
//                exit(1);
//            }
//
//
//            if ( begin < 0 )
//            {
//                cerr  << "BEGIN [" << begin << "] IS SMALLER THAN 0." << endl
//                      << "ALTHOUGH I COULD DEAL WITH THAT, I'VE "
//                      << "CHOOSEN TO DIE AND LET YOU KNOW THAT YOUR DATA IS "
//                      << "WRONG" << endl;
//                exit(2);
//            }
//
//
//            if ( iMax == 0 )
//            {
//                int iArrSize = (int) svLines->size();
//                if ( iArrSize < end   )
//                {
//                    svLines->resize(end + 1, 0);
//                }
//            } else {
//                if ( end > iMax )
//                {
//                    cerr    << "END [" << end << "] IS GREATER THAN MAXIMUM ["
//                            << iMax << "] PASSED IN THE COMMAND LINE." << endl;
//                    exit(3);
//                }
//            }
//
//
//            if ( end > lastEnd ) {
//                lastEnd = end;
//            }
//
//            for ( int i = begin; i <= end; ++i )
//            {
//                ++(*svLines)[i];
//            }
//
//            //cout << cLine << " BEGIN " << begin << " END " << end << endl;
//        } else {
//            //cerr << "empty line : " << cLine << endl;
//            //pass
//        }
//    };
//
//    int iArrSize = (int) svLines->size();
//
//    if ( gap )
//    {
//        //cerr << "CLOSING GAP BETWEEN " << lastEnd << " AND " << iArrSize << "\n";
//        for ( int i = lastEnd; i < iArrSize; ++i )
//        {
//            //closes with 1 when needed a reverse (1-n) probability
//            (*svLines)[i] = 1;
//        }
//    }
//
//    for ( int i = 0; i < iArrSize; ++i )
//    {
//        unsigned short val = (*svLines)[i];
//        //int val = (*svLines)[i];
//        //cout << i << "\t" << val << endl;
//        cout << i << "\t" << val << "\n";
//        //cout << i << "\t" << val << terminal;
//    }
//
//    return 0;
//
//}
